var mqtt = require('mqtt');
const inquirer = require("inquirer");
const chalk = require("chalk");
const figlet = require("figlet");
const shell = require("shelljs");
const notifier = require('node-notifier');
const path = require('path');

var ip_broker = "tcp://127.0.0.1:1883";
var client = mqtt.connect(ip_broker);

var subscribedTopics = ["General"];

//region MQTT CLIENT METHODS
const connectMqttBroker = () => {
  client.on('connect', function() {
    subscribedTopics.forEach((topic) => {
      client.subscribe(topic)
    });
    onReceiveMessages()
  })
};

const disconnectMqttBroker = () => {
  client.end()
};

const onReceiveMessages = () => {
  client.on('message', function (topic, message){
    notifier.notify({
      title: topic.toString(), 
      message: message.toString(), 
      icon: path.join(__dirname, "uag_logo.png"), 
      sound: true
    })
  })
};

const subscribeOnTopic = (topic) => new Promise((resolve, reject) => {
  client.subscribe(topic, function(err) {
    if(!err) {
      console.log();
      console.log(chalk.hex("#FFFFFF").inverse(`You are subscribed successfully on ${topic}`));
      console.log();
      subscribedTopics.push(topic);
      resolve(true)
    } else {
      console.log();
      console.log(chalk.red.inverse(`There was a problem trying to subscribe on ${topic}`));
      console.log();
      reject(false)
    }
  })
});

const unsubscribeOnTopic = (topic) => new Promise((resolve, reject) => {
  client.unsubscribe(topic, function(err) {
    if(!err) {
      console.log();
      console.log(chalk.hex("#FFFFFF").inverse(`You are unsubscribed successfully on ${topic}`));
      console.log();
      topicIndex = subscribedTopics.indexOf(topic);
      if(topicIndex != -1) {
        subscribedTopics.splice(topicIndex, 1)  
      }
      resolve(true)
    } else {
      console.log();
      console.log(chalk.red.inverse(`There was a problem trying to subscribe on ${topic}`));
      console.log();
      reject(false)
    }
  })
});

const publishOnTopic = (topic, message) => client.publish(topic, message);
//endregion

//region TERMINAL METHODS
const showSubscribedTopics = () => {
  console.log("");
  if(subscribedTopics.length > 0){
    console.log(chalk.hex("#FFFFFF").inverse("You are subscribed on the following topics:"));
    subscribedTopics.forEach((topic) => {
      console.log(chalk.green.inverse(`> ${topic}`))
    })
  } else {
    console.log(chalk.hex("#FFFFFF").inverse("There are not subscribed topics!"))
  }
  console.log("")
};

const showScriptTitle = () => {
  console.log(
    chalk.green(
      figlet.textSync("Mqtt UAG Client", {
        horizontalLayout: "default",
        verticalLayout: "default"
      })
    )
  );
};

const showMenuOptions = () => {
  const questions = [
    {
      type: "list",
      name: "SELECTED_OPTION",
      message: "what can I do for you?",
      choices: ["1.subscribe on topic", "2.unsubscribe on topic", "3.publish on topic", "4.Show subscribed Topics", "5.exit"],
      filter: function(val) {
        return val.split(".")[1];
      }
    }
  ];
  return inquirer.prompt(questions);
};

const showSubcribtionWizard = () => {
  const questions = [
    {
      type: "name",
      name: "TOPIC_NAME",
      message: "What is the name of Topic to subscribe?"
    }
  ];
  return inquirer.prompt(questions);
};

const showUnsubscriptionTopicWizard = () => {
  const questions = [
    {
      type: "list",
      name: "TOPIC_NAME",
      message: "Which of following topics do you want unsubscribe?",
      choices: subscribedTopics
    }
  ];
  return inquirer.prompt(questions)
};

const showPublishOnTopicWizard_listTopics = () => {
  const questions = [
    {
      type: "list",
      name: "TOPIC_NAME",
      message: "Which of following topics do you want use to publish?",
      choices: [...subscribedTopics, "Other topic"]
    }
  ];
  return inquirer.prompt(questions)
};

const showPublishOnTopicWizard_askForTopicName = () => {
  const questions = [
    {
      type: "name",
      name: "TOPIC_NAME",
      message: "Enter the name of the topic to publish:"
    }
  ];
  return inquirer.prompt(questions)
};

const showPublishOnTopicWizard_askForMessage = () => {
  const questions = [
    {
      type: "name",
      name: "PUBLISH_MESSAGE",
      message: "What is the message that you want to publish?"
    }
  ];
  return inquirer.prompt(questions)
};

//endregion

//region MAIN
const run = async () => {
  showScriptTitle();
  connectMqttBroker();
  while(true){
    const menuAnswer = await showMenuOptions();
    switch(menuAnswer.SELECTED_OPTION) {
      case "subscribe on topic":
        const newTopicName = await showSubcribtionWizard();
        const subscribtionResponse = await subscribeOnTopic(newTopicName.TOPIC_NAME);
        break;
      case "unsubscribe on topic":
        const topicToDelete = await showUnsubscriptionTopicWizard();
        const unsubscribtionResponse = await unsubscribeOnTopic(topicToDelete.TOPIC_NAME);
        break;
      case "publish on topic":
        let topic = await showPublishOnTopicWizard_listTopics();
        if(topic.TOPIC_NAME === "Other topic"){
          topic = await showPublishOnTopicWizard_askForTopicName();
        }
        const message = await showPublishOnTopicWizard_askForMessage();
        publishOnTopic(topic.TOPIC_NAME, message.PUBLISH_MESSAGE);
        break;
      case "Show subscribed Topics":
        showSubscribedTopics();
        break;
      case "exit":
        disconnectMqttBroker();
        process.exit();
        return;
      default:
        console.log(`selected option not recognized`);
        return;
    }
  }
};

run();

//endregion